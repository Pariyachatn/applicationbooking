package com.example.applicationbarberbooking;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity{
    EditText username, ed_email, ed_password;
    Button btnRegis;
    ProgressBar loading;
    TextView txtLogin;
    private FirebaseAuth mAuth;
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        loading = findViewById(R.id.progressBar);
        username = findViewById(R.id.username);
        ed_email = findViewById(R.id.ed_email);
        ed_password = findViewById(R.id.ed_password);
        btnRegis = findViewById(R.id.regis);
        txtLogin = findViewById(R.id.txtLogin);
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser()!=null) mAuth.signOut();

        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("check", "onClick: success ");
                mAuth.createUserWithEmailAndPassword(ed_email.getText().toString(), ed_password.getText().toString())
                        .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(Task<AuthResult> task) {
                                if (task.isSuccessful()){
                                    final Map<String, Object> customer = new HashMap<>();
                                    customer.put("uId",mAuth.getCurrentUser().getUid());
                                    customer.put("name",username.getText().toString())  ;
                                    customer.put("email",ed_email.getText().toString()) ;
                                    customer.put("password",ed_password.getText().toString());
                                    Log.d("regis", "onComplete: success");
                                    Log.d("Check", "onCreate: "+ mAuth.getCurrentUser().getUid());
                                    db.collection("User").add(customer)
                                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                                @Override
                                                public void onSuccess(DocumentReference documentReference) {
                                                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                                                    startActivity(intent);
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("Error", "Error writing document", e);
                                                }
                                            });
                                }
                                else {
                                    Toast.makeText(RegisterActivity.this, "Faild", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.e("Eror", "onFailure: "+ e.getMessage());
                    }
                });
            }
        });
    }
}


