package com.example.applicationbarberbooking;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MyTimeSlotAdapter extends RecyclerView.Adapter<MyTimeSlotAdapter.MyViewHolder> {
    Context context;
    List<TimeSlot> timeSlotList;
    List<CardView> cardViewList;
    LocalBroadcastManager localBroadcastManager;
    FirebaseFirestore db;

    public MyTimeSlotAdapter(Context context) {
        this.context = context;
        this.timeSlotList = new ArrayList<>();
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
        cardViewList = new ArrayList<>();
    }

    public MyTimeSlotAdapter(Context context, List<TimeSlot> timeSlotList) {
        this.context = context;
        this.timeSlotList = timeSlotList;
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
        cardViewList = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_timeslot, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {

        myViewHolder.SetItem(timeSlotList.get(i));
//        myViewHolder.txt_time_slot.setText(new StringBuilder(Common.convertTimeSlotToString(i)).toString());
//        if (timeSlotList.size() == 0) {
//
//            myViewHolder.card_time_slot.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
//            myViewHolder.txt_slot_description.setText("Available");
//            myViewHolder.txt_slot_description.setTextColor(context.getResources().getColor(android.R.color.black));
//            myViewHolder.txt_time_slot.setTextColor(context.getResources().getColor(android.R.color.black));
//
//        } else {
//            for (TimeSlot slotValue : timeSlotList) {
////                int slot = Integer.parseInt(slotValue.getSlot().toString());
////                if (slot == i) {
////                    myViewHolder.card_time_slot.setTag(Common.DISABLE_TAG);
////                    myViewHolder.card_time_slot.setCardBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));
////                    myViewHolder.txt_slot_description.setText("Full");
////                    myViewHolder.txt_slot_description.setTextColor(context.getResources()
////                            .getColor(android.R.color.white));
////                    myViewHolder.txt_time_slot.setTextColor(context.getResources().getColor(android.R.color.white));
////                }
//            }
//        }
//
//        if (!cardViewList.contains(myViewHolder.card_time_slot))
//            cardViewList.add(myViewHolder.card_time_slot);
//
//        myViewHolder.setiRecyclerItemSelectListner(new IRecyclerItemSelectListner() {
//            @Override
//            public void onItemSelcetListner(View view, int pos) {
//                for (CardView cardView : cardViewList) {
//                    if (cardView.getTag() == null)
//                        cardView.setCardBackgroundColor(context.getResources()
//                                .getColor(android.R.color.white));
//                }
//                myViewHolder.card_time_slot.setCardBackgroundColor(context.getResources()
//                        .getColor(R.color.colorYello));
//                Intent intent = new Intent(context,ConfirmBooking.class);
//                context.startActivity(intent);
//                Map<String,Object> time = new HashMap<>();
////                time.put("DateTimeStart",intent.putExtra(Common.KEY_TIME_SLOT,i));
//
//                db.collection("Booking").add(time).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.d("time", "onSuccess: "+documentReference.getId());
//                    }
//                });
//
//
//                localBroadcastManager.sendBroadcast(intent);
//
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return timeSlotList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_time_slot,txt_slot_description;
        CardView card_time_slot;
        IRecyclerItemSelectListner iRecyclerItemSelectListner;
        TimeSlot timeSlot;

        public void setiRecyclerItemSelectListner(IRecyclerItemSelectListner iRecyclerItemSelectListner) {
            this.iRecyclerItemSelectListner = iRecyclerItemSelectListner;
        }

        public MyViewHolder(View itemView){
            super(itemView);
            card_time_slot = (CardView)itemView.findViewById(R.id.card_time_slot);
            txt_time_slot = (TextView)itemView.findViewById(R.id.txt_time_slot);
            txt_slot_description = (TextView)itemView.findViewById(R.id.txt_time_slot_avaible);




            itemView.setOnClickListener(this);
        }

        public void  SetItem(final TimeSlot timeSlot){
            this.timeSlot = timeSlot;
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            txt_time_slot.setText(df.format(timeSlot.StartTime) + " - "+df.format(timeSlot.StopTime));
            txt_slot_description.setText(timeSlot.IsAvaible ? "Avaible" : "Full");

            card_time_slot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.currentTimeSlot = timeSlot;
                    Intent intent = new Intent(context,ConfirmBooking.class);
                    context.startActivity(intent);
                }
            });


        }

        @Override
        public void onClick(View v) {
            iRecyclerItemSelectListner.onItemSelcetListner(v,getAdapterPosition());
        }
    }

}
