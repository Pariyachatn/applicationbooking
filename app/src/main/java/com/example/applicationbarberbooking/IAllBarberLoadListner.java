package com.example.applicationbarberbooking;

import java.util.List;

public interface IAllBarberLoadListner {
    void onAllBarberLoadListSuccess(List<Barber> barberList);
    void onAllBarberLoadListFailed(String message);
}
