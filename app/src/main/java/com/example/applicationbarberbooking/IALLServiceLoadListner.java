package com.example.applicationbarberbooking;

import java.util.List;

public interface IALLServiceLoadListner {
    void onAllServiceLoadListSuccess(List<Service> areaNameList);
    void onAllServiceLoadListFailed(String message);
}
