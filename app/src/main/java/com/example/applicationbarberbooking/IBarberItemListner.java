package com.example.applicationbarberbooking;

import java.util.List;

public interface IBarberItemListner {
    void onBarberItemLoadSuccess(List<BarberItem> barberItemList );
    void onBarberItemLoadFailed(String meassage);
}
