package com.example.applicationbarberbooking;

public class listItembarber {
    String name,position,img,phone;
    public listItembarber(){

    }
    public listItembarber(String name,String position,String img, String phone){
        this.name = name;
        this.position = position;
        this.img = img;
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}
