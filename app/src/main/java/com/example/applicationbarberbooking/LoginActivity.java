package com.example.applicationbarberbooking;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends AppCompatActivity {
    EditText ed_email, ed_password;
    Button btnLogin;
    TextView txtRegis;
    ProgressBar loadbar;
    FirebaseAuth fAuth;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ed_email = findViewById(R.id.phone);
        ed_password = findViewById(R.id.password);
        loadbar = findViewById(R.id.loading);
        fAuth = FirebaseAuth.getInstance();

        btnLogin = findViewById(R.id.login);
        txtRegis = findViewById(R.id.txtRegis);

        txtRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String email = ed_email.getText().toString().trim();
                String password = ed_password.getText().toString().trim();
                if (TextUtils.isEmpty(email)){
                    ed_email.setError("Phone is Reduired");
                    return;
                }
                if (TextUtils.isEmpty(password)){
                    ed_password.setError("Password is Reduired");
                    return;
                }

                if (password.length() < 8){
                    ed_password.setError("Password Must be >= 8 Character");
                    return;
                }

                loadbar.setVisibility(View.VISIBLE);

                fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            db = FirebaseFirestore.getInstance();
                            Toast.makeText(LoginActivity.this,"Login in Success", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),SelectService.class));
                        }else {
                            Toast.makeText(LoginActivity.this,"Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });


    }
}