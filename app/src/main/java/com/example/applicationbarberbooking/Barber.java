package com.example.applicationbarberbooking;

import android.os.Parcel;
import android.os.Parcelable;

public class Barber implements Parcelable{
    private String name,position,id;

    public Barber(String id, String name){
        this.id = id;
        this.name = name;
    }


    protected Barber(Parcel in) {
        name = in.readString();
        id = in.readString();
        position = in.readString();
    }

    public static final Creator<Barber> CREATOR = new Creator<Barber>() {
        @Override
        public Barber createFromParcel(Parcel in) {
            return new Barber(in);
        }

        @Override
        public Barber[] newArray(int size) {
            return new Barber[size];
        }
    };

    public String getBarberId() {
        return id;
    }

    public void setBarberId(String barberId) {
        this.id = barberId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(position);

    }

}
