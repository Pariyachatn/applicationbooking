package com.example.applicationbarberbooking;

public class Customer {
    public String uId, name,email,password;

    public Customer(){

    }
    public Customer(String uId,String username,String email,String password){
        this.uId = uId;
        this.name = username;
        this.email = email;
        this.password = password;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uid) {
        this.uId = uid;
    }

    public String getUserName() {
        return name;
    }

    public void setUserName(String userName) {
        this.name = userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
