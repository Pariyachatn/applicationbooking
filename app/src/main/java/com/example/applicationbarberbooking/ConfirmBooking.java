package com.example.applicationbarberbooking;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ConfirmBooking extends AppCompatActivity {
    SimpleDateFormat simpleDateFormat;
    LocalBroadcastManager localBroadcastManager;
    FirebaseFirestore db;
    FirebaseAuth mAuth;
    DocumentReference userRef;

    Unbinder unbinder;
    TextView txt_nameUser;


    TextView txt_barber;

    TextView txt_time;

    @OnClick(R.id.btn_confirm)
    void confirmBooking(){
        db.collection("User").whereEqualTo("uId",mAuth.getCurrentUser().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            Log.d("User", "onComplete: "+task.getResult().size());

                            DocumentSnapshot queryDocumentSnapshot = task.getResult().getDocuments().get(0);

                            Log.d("uid", queryDocumentSnapshot.getId());
                            Log.d("uid",task.getResult().getDocuments().size()+"");

                            BookingInformation bookingInformation = new BookingInformation();
                            bookingInformation.cusId = queryDocumentSnapshot.getId();
                            bookingInformation.barberId = Common.currentBarber.getBarberId();
                            bookingInformation.serviceId = Common.currentService.getId();
                            bookingInformation.dateTimeStart =  Common.currentTimeSlot.StartTime;
                            bookingInformation.dateTimeStop = Common.currentTimeSlot.StopTime;
                            bookingInformation.status = "Wait";

                            FirebaseFirestore bookingData = FirebaseFirestore.getInstance();
                            bookingData.collection("Booking").add(bookingInformation)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            ConfirmBooking.this.finish();
                                            Toast.makeText(ConfirmBooking.this,"Success",Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(ConfirmBooking.this,ShowBooking.class));
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure( Exception e) {
                                    Log.d("Error", "onFailure: "+ "" + e.getMessage());
                                    Toast.makeText(ConfirmBooking.this,"" + e.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                    }
                });

    }

    BroadcastReceiver confirmBookingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setData();
        }
    };

    private void setData() {
        txt_barber = findViewById(R.id.txt_barber);
        txt_barber.setText(Common.currentBarber.getName());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);
        txt_nameUser = findViewById(R.id.txt_nameUser);
        txt_barber = findViewById(R.id.txt_barber);
        txt_time = findViewById(R.id.txt_time);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(confirmBookingReceiver,new IntentFilter(Common.KEY_CONFIRM_BOOKING));

        unbinder = ButterKnife.bind(this);
        showUsername();
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(confirmBookingReceiver);
        super.onDestroy();
    }
    private void showUsername() {
        db.collection("User").whereEqualTo("uId",mAuth.getCurrentUser().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            Log.d("User", "onComplete: "+task.getResult().size());

                            DocumentSnapshot queryDocumentSnapshot = task.getResult().getDocuments().get(0);

                            Log.d("uid", queryDocumentSnapshot.getId());
                            Log.d("uid",task.getResult().getDocuments().size()+"");

                            txt_nameUser.setText(queryDocumentSnapshot.getData().get("name").toString());
                            txt_barber.setText(Common.currentBarber.getName());
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                            txt_time.setText(simpleDateFormat.format(Common.currentTimeSlot.StartTime));



                        }

                    }
                });

    }
}