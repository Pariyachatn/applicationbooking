package com.example.applicationbarberbooking;

import com.google.firebase.firestore.DocumentReference;

import java.util.Date;

public class BookingInformation {
    public String cusId,barberId,serviceId,status;
    public Date dateTimeStart,dateTimeStop;

    public BookingInformation(){

    }

    public  BookingInformation(String Customer, String Barber,String serviceId, Date DateTimeStart, Date DateTimeStop, String Status ){
        this.cusId = Customer;
        this.barberId = Barber;
        this.serviceId = serviceId;
        this.dateTimeStart = DateTimeStart;
        this.dateTimeStop = DateTimeStop;
        this.status =Status;
    }
}
