package com.example.applicationbarberbooking;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyBarberAdapter extends RecyclerView.Adapter<MyBarberAdapter.MyViewHolder> {

    Context context;
    List<Barber> barberList;
    List<CardView> cardViewList;
    LocalBroadcastManager localBroadcastManager;

    public MyBarberAdapter(Context context,List<Barber> barberList){
        this.context = context;
        this.barberList = barberList;
        cardViewList = new ArrayList<>();
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_barbers,viewGroup,false);
        return new MyBarberAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {
        myViewHolder.SetItem(barberList.get(i));
    }
    @Override
    public int getItemCount() {
        return barberList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_barber_name;
        CardView card_barber;
        Barber barber;


        IRecyclerItemSelectListner iRecyclerItemSelectListner;


        public void setiRecyclerItemSelectListner(IRecyclerItemSelectListner iRecyclerItemSelectListner) {
            this.iRecyclerItemSelectListner = iRecyclerItemSelectListner;
        }

        public MyViewHolder(View itemView) {
            super(itemView);
            card_barber = (CardView)itemView.findViewById(R.id.card_barber);
            txt_barber_name = (TextView)itemView.findViewById(R.id.txt_barberName);
            Fragment fragment;

            card_barber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.currentBarber = barber;
                    Log.d("berber", "onClick: "+barber.getName());
                    Intent intent = new Intent(context,SelectDateTime.class);
                    context.startActivity(intent);
                }
            });


        }

        public void SetItem(Barber barber){
            this.barber = barber;
            txt_barber_name.setText(barber.getName());
        }

        @Override
        public void onClick(View v) {
        }
    }
}
