package com.example.applicationbarberbooking;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import dmax.dialog.SpotsDialog;

public class SelectDateTime extends AppCompatActivity {

    DocumentReference bookingRef;
    DocumentReference date;
    ITimeSlotLoadListener iTimeSlotLoadListener;
    AlertDialog dialog;

    Unbinder unbinder;
    LocalBroadcastManager localBroadcastManager;
    List<TimeSlot> timeSlots = new ArrayList<>();
    List<Booking> Bookings = new ArrayList<>();

    FirebaseFirestore db;


    @BindView(R.id.recyclerTime)
    RecyclerView recyclerView;
    @BindView(R.id.calendarView)
    HorizontalCalendarView calendarView;
    SimpleDateFormat simpleDateFormat;
    BroadcastReceiver displayTimeSlot = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Calendar date = Calendar.getInstance();
            date.add(Calendar.DATE, 0);
            loadAvailbleTimeSlotOfBarber(simpleDateFormat.format(date.getTime()));
        }
    };

    private void loadAvailbleTimeSlotOfBarber(final String bookdate) {
        timeSlots.clear();
        dialog.show();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date_time);

        db = FirebaseFirestore.getInstance();


        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(displayTimeSlot, new IntentFilter(Common.KEY_DISPLAY_TIME_SLOT));

        simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");

        dialog = new SpotsDialog.Builder().setContext(this).setCancelable(false).build();

        unbinder = ButterKnife.bind(this);

        init(this);
        initTimeSlot();
    }


    private void initTimeSlot() {
        dialog.show();
        db.collection("Booking").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    Bookings.clear();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Booking booking = new Booking();

                        Log.d("doc", "onComplete: "+document);

                        Timestamp dateTimeStart = ((Timestamp) document.getData().get("dateTimeStart"));
                        Timestamp dateTimeStop = ((Timestamp) document.getData().get("dateTimeStop"));

                        booking.DateTimeStart = dateTimeStart==null ? null : dateTimeStart.toDate();
                        booking.DateTimeStop = dateTimeStop==null ? null : dateTimeStop.toDate();
                        booking.Status = (String) document.getData().get("status");
                        Bookings.add(booking);
                    }

                    for (TimeSlot timeSlot : Common.TimeList) {
                        timeSlot.StartTime.setDate(Common.currentDate.get(Calendar.DATE));
                        timeSlot.StartTime.setMonth(Common.currentDate.get(Calendar.MONTH));
                        timeSlot.StartTime.setYear(Common.currentDate.get(Calendar.YEAR) - 1900);

                        timeSlot.StopTime.setDate(Common.currentDate.get(Calendar.DATE));
                        timeSlot.StopTime.setMonth(Common.currentDate.get(Calendar.MONTH));
                        timeSlot.StopTime.setYear(Common.currentDate.get(Calendar.YEAR) - 1900);


                        timeSlot.IsAvaible = true;

                        for (Booking booking : Bookings) {
                            if (booking.DateTimeStart!=null && booking.DateTimeStop!=null && booking.DateTimeStart.compareTo(timeSlot.StartTime)==0 && booking.DateTimeStop.compareTo(timeSlot.StopTime) == 0) {
                                timeSlot.IsAvaible = false;
                            }
                        }
                    }

                    recyclerView.setHasFixedSize(true);
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                    recyclerView.setLayoutManager(gridLayoutManager);
//        recyclerView.addItemDecoration(new SpacesItemDecoration(8));
                    MyTimeSlotAdapter adapter = new MyTimeSlotAdapter(getApplicationContext(), Common.TimeList);
                    recyclerView.setAdapter(adapter);
                    dialog.dismiss();
                }
            }
        });
    }

    private void init(SelectDateTime selectDateTime) {
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE, 1);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(1)
                .mode(HorizontalCalendar.Mode.DAYS)
                .defaultSelectedDate(startDate)
                .build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                if (Common.currentDate.getTimeInMillis() != date.getTimeInMillis()) {
                    Common.currentDate = date;
                    initTimeSlot();

                    loadAvailbleTimeSlotOfBarber(simpleDateFormat.format(date.getTime()));
                    dialog.dismiss();
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(displayTimeSlot);
        super.onDestroy();
    }
}
