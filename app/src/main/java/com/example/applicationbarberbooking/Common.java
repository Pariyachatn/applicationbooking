package com.example.applicationbarberbooking;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Common {
    public  static final int TIME_SLOT_TOTAL = 10;
    public static final String KEY_DISPLAY_TIME_SLOT = "DISPLAY TIME SLOT";
    public static final String KEY_ENABLE_BUTTON_NEXT = "KEY_ENABLE_BUTTON_NEXT" ;
    public static final String KEY_BARBER_STORE = "BARBER_SAVE";
    public static final String KEY_BARBER_LOAD_DONE = "KEY_BARBER_LOAD_DONE";
    public static final String KEY_STEP = "STEP" ;
    public static final String KEY_BARBER_SELECTED = "BARBER_SELCETED" ;
    public static final Object DISABLE_TAG = "DISABLE" ;
    public static final String KEY_TIME_SLOT = "TIME_SLOT" ;
    public static final String KEY_CONFIRM_BOOKING = "CONFIRM_BOOKING" ;
    public static final String KEY_DISPLAY_BARBER = "DISPLAY_BARBER";
    public static Barber currentBarber;
    public static int step = 0;
    public static String barbername = "" ;
    public static TimeSlot currentTimeSlot;
    public static Calendar currentDate=Calendar.getInstance();
    public static String barberId = "";
    public static Service currentService;
    public static Customer currentUser;
    public static List<TimeSlot> TimeList = new ArrayList<TimeSlot>(){
        {
            int[] time1 = {10,11};
            while (time1[1]<=13){
                add(new TimeSlot(new Date(0,0,0,time1[0],0,0),new Date(0,0,0,time1[1],0,0),true));
                time1[0]+=1;
                time1[1]+=1;
            }

            int[] time2 = {14,15};
            while(time2[1]<=21){
                add(new TimeSlot(new Date(0,0,0,time2[0],0,0),new Date(0,0,0,time2[1],0,0),true));
                time2[0]+=1;
                time2[1]+=1;
            }
        }
    };



}
