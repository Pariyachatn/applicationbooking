package com.example.applicationbarberbooking;

import java.sql.Timestamp;
import java.util.Date;

public class Booking {
    public String Customer;
    public String Barber;
    public Date DateTimeStart;
    public Date DateTimeStop;
    public String Status;

    public Booking() {

    }

    public Booking(Date DateTimeStart, Date DateTimeStop, String Status) {
//        this.Customer= Customer;
//        this.Barber =Barber;
        this.DateTimeStart = DateTimeStart;
        this.DateTimeStop = DateTimeStop;
        this.Status = Status;

    }

}
