package com.example.applicationbarberbooking;

import java.util.Date;
import java.util.ArrayList;

public class TimeSlot {
    public Date StartTime;
    public Date StopTime;
    public boolean IsAvaible;
    public TimeSlot(Date startTime,Date stopTime,boolean isAvaible){
        this.StartTime = startTime;
        this.StopTime = stopTime;
        this.IsAvaible = isAvaible;
    }
}
