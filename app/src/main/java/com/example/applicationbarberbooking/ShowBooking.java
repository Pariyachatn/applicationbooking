package com.example.applicationbarberbooking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;

public class ShowBooking extends AppCompatActivity {
    TextView txtcusname,txtbarbername,datetimeStart,datetimeStop;
    FirebaseAuth mAuth;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_booking);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        txtcusname = (TextView) findViewById(R.id.cusName);
        txtbarbername = (TextView) findViewById(R.id.barberName);
        datetimeStart = (TextView) findViewById(R.id.datetimeStart);
        datetimeStop = (TextView) findViewById(R.id.datetimeStop);

        showDetailBooking();
    }

    private void showDetailBooking() {
        db.collection("User").whereEqualTo("uId",mAuth.getCurrentUser().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            Log.d("User", "onComplete: "+task.getResult().size());

                            DocumentSnapshot queryDocumentSnapshot = task.getResult().getDocuments().get(0);

                            Log.d("uid", queryDocumentSnapshot.getId());
                            Log.d("uid",task.getResult().getDocuments().size()+"");

                            txtcusname.setText(queryDocumentSnapshot.getData().get("name").toString());
                            txtbarbername.setText(Common.currentBarber.getName());
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                            datetimeStart.setText(simpleDateFormat.format(Common.currentTimeSlot.StartTime));
                            datetimeStop.setText(simpleDateFormat.format(Common.currentTimeSlot.StopTime));
                        }

                    }
                });

    }
}