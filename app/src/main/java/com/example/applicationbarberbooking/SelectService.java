package com.example.applicationbarberbooking;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;


public class SelectService extends AppCompatActivity implements IALLServiceLoadListner {

    CollectionReference serviceRef;
    CollectionReference barberRef;
    FirebaseFirestore db;
    FirebaseAuth mAuth;

    IALLServiceLoadListner iallServiceLoadListner;

    Spinner spinner;
    @BindView(R.id.txtUsername)
    TextView txtusername;
    @BindView(R.id.listviewBarber)
    RecyclerView listnameBarber;

    List<Service> listService = new ArrayList<>();
    List<Barber> listBarber = new ArrayList<>();


    Unbinder unbinder;
    AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_service);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        iallServiceLoadListner = this;
        serviceRef = FirebaseFirestore.getInstance().collection("Service");
        dialog = new SpotsDialog.Builder().setContext(this).build();
        spinner = findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Common.currentService = listService.get(position);
                Log.d("test", "onItemSelected: " + listService.get(position).id);
                loadBarberOfService(listService.get(position).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        unbinder = ButterKnife.bind(this);
        showUsername();
        loadAllService(this);

    }

    private void initView() {
        listnameBarber.setHasFixedSize(true);
        listnameBarber.setLayoutManager(new LinearLayoutManager(this));
//        listnameBarber.addItemDecoration(new SpacesItemDecoration(4));
        MyBarberAdapter adapter = new MyBarberAdapter(this, listBarber);
        listnameBarber.setAdapter(adapter);
    }

    private void showUsername() {
        db.collection("User").whereEqualTo("uId",mAuth.getCurrentUser().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            Log.d("User", "onComplete: "+task.getResult().size());

                            DocumentSnapshot queryDocumentSnapshot = task.getResult().getDocuments().get(0);

                            Log.d("uid", queryDocumentSnapshot.getId());
                            Log.d("uid",task.getResult().getDocuments().size()+"");

                            txtusername.setText(queryDocumentSnapshot.getData().get("name").toString());


                        }

                    }
                });
    }

    private void loadAllService(final Context context) {
        listService.clear();
        db = FirebaseFirestore.getInstance();
        db.collection("Service").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                                listService.add(new Service(documentSnapshot.getId(), documentSnapshot.getData().get("name").toString()));
                            }
                            iallServiceLoadListner.onAllServiceLoadListSuccess(listService);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                iallServiceLoadListner.onAllServiceLoadListFailed(e.getMessage());
            }
        });
    }

    @Override
    public void onAllServiceLoadListSuccess(List<Service> areaNameList) {
        ArrayAdapter<Service> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, areaNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onAllServiceLoadListFailed(String message) {

    }

    private void loadBarberOfService(String id) {
        dialog.show();
        Log.d("name", "loadBarberOfService: " + id);
        barberRef = FirebaseFirestore.getInstance()
                .collection("Service/"+id+"/Barber");

        barberRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(Task<QuerySnapshot> task) {
                listBarber.clear();
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                        listBarber.add(new Barber(documentSnapshot.getId(), documentSnapshot.getData().get("name").toString()));
                        Log.d("bid", "onComplete: "+documentSnapshot.getId()+" "+documentSnapshot.getData().get("name").toString());
                    }

                    initView();
                }
                dialog.dismiss();
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
